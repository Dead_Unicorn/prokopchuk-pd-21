﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//1Виводить на екран введене число з клавіатури в зворотному порядку (1234->4321)
//2Виводить будь-яку строку в зворотному порядку (АБВ->ВБА)
//3Дробові числа виводяться в зворотному порядку і ціла частина і дробова (123.456->321.654)
//4Виводити будь-яку строку в зворотному порядку і всі елементи після “магічного знаку” теж в зворотному (АБВ, ГДЕ->ВБА, ЕДГ)
//5Реалізувати пункти 1-4 за допомогою методів, перевантаживши методи для різних типів
//6Реалізувати пункти 1-4 за допомогою рекурсії, методи для різних типів перевантажити
//7Реалізувати метод, що буде масив повертати задом навпаки (Використання Array.Reverse() заборонено!)
//8Виконати пункт 7 з використанням ключових слів ref i out

namespace Lab6
{
     public class Program
    {

        static void First()
        {
            Console.WriteLine("Enter:");
            string str = Console.ReadLine();

            for (int i = 0; i < str.Length; ++i)
            {
                Console.Write(str[str.Length - i - 1]);
            }
        }
        static void Second()
        {
            Console.WriteLine("Enter:");
            string str = Console.ReadLine();

            for (int i = 0; i < str.Length; ++i)
            {
                Console.Write(str[str.Length - i - 1]);
            }
        }
        static void Third(double d = 123.456)
        {
            var strL = d.ToString().Split('.', ',');
            string str1 = new string(strL[0].Reverse().ToArray());
            string str2 = new string(strL[1].Reverse().ToArray());
            Console.WriteLine(str1 + "." + str2);
        }
        static void Fourth()
        {
            Console.WriteLine("Enter:");
            string str = Console.ReadLine();
            Console.WriteLine("Enter the magic character: ");
            char magicChar = Char.Parse(char.ConvertFromUtf32(Console.Read()));
            string[] res1 = str.Split(magicChar);
            for (int i = 0; i < res1.Length; ++i)
            {
                res1[i] = new string(res1[i].Reverse().ToArray());
            }
            Console.WriteLine(res1[0] + magicChar + res1[1]);
        }
        static void MyReverse<T>(ref T[] input,out T[] output)
        {
            output = new T[input.Length];
            for(int i =  input.Length - 1,j=0; i > -1; --i,j++ )
            {
                output[j] = input[i];
                //output.SetValue(input[i], j);
            }
        }
        static void RecorsiveFirst()
        {
            int number;
            number = Convert.ToInt32(Console.ReadLine());
            if(number != 0)
            {
                RecorsiveFirst();
                Console.Write(number + " ");
            }
        }
        static void Second(string str)
        {
            if(str != string.Empty)
            {
                Second(str.Substring(1));
                Console.Write(str[0]);
            }
        }
        static void ThirdRecorsive(double d = 123.456)
        {
            var strL = d.ToString().Split('.', ',');
            Second(strL[0]);
            Console.Write('.');
            Second(strL[1]);
        }
        static void Fourth(string str,char magicChar)
        {
            string[] res1 = str.Split(magicChar);
            Second(res1[0]);
            Console.Write(magicChar);
            Second(res1[1]);
        }
        static void Main(string[] args)
        {
            
        }
    }
}
