﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Дані координати(як цілі від 1 до 8) двох різних полів шахівниці. Якщо король за один хід може перейти з одного поля на інше, вивести логічне значення True, інакше вивести значення False.*/
            int x;
            int y;
            Console.WriteLine("ВВедите x и y (от 1 до 8)");
            do
            {
                Console.WriteLine("x: ");
                x = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("y: ");
                y = Convert.ToInt32(Console.ReadLine());
            } while (x > 8 || x < 1 || y > 8 || y < 1);

            if(x + 1 > 8 || x - 1 < 1 || y + 1 > 8 || y - 1 < 1)
                Console.WriteLine("FALSE");
            else Console.WriteLine("TRUE");
        }
    }
}
