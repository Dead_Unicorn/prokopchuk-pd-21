﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//1.Створити клас Cars, в якому міститься набір з 5 машин німецького виробництва, для кожної з машин визначені 4характеристики: колекція кольорів в якому вона представлена, рік випуску і ціна. Четверту характеристику оберіть самі. Реалізувати можливість для покупця обрати машину з набору за її параметрами. Повідомити якщо не існує машини з потрібними параметрами, якщо існує – вивести на екран повний опис машини.
namespace Самостоятельная
{

    public class Car
    {
        public string Color { get; set; }
        public int Year { get; set; }
        public double Price { get; set; }
        public string Name{ get; set; }
        public override string ToString()
        {
            return $"Name: {Name}, Year: {Year}, Price: {Price}, Color: {Color}";
        }
    }
    public static class Cars
    {
        public static List<Car> CarsList { get; }
        static Cars()
        {
            CarsList = new List<Car>
            {
                new Car{ Name="BMW x5",Price=9000,Color="White",Year=2006 },
                new Car{ Name="BMW x7",Price=86500,Color="Black",Year=2019 },
                new Car{ Name="BMW Z4",Price=60000,Color="Red",Year=2019 },
                new Car{ Name="Mercedes-Benz G500",Price=26000,Color="Black",Year=2015 },
                new Car{ Name="Mercedes E-Class Coupe (C238)",Price=65000,Color="Red",Year=2020 },
                new Car{ Name="Mercedes E-Class (W213)",Price=60000,Color="White",Year=2020 }
            };
        }
        public static List<Car> FindCars(string Name = "", string Color = "", int Year = 0, double Price=0)
        {
            List<Car> res = new List<Car>();
            if(CarsList.Contains(new Car { Name = Name, Color = Color, Year = (int)Year, Price = (double)Price }))
            {
                res.Add(new Car { Name = Name, Color = Color, Year = (int)Year, Price = (double)Price });
                Console.WriteLine(res[0].ToString());
                return res;
            }

            res = CarsList.Select(x=>x).Where(x => x.Name == Name || x.Price == Price || x.Year == Year || x.Color == Color).ToList();
            foreach(var i in res)
                Console.WriteLine(i);

            return res;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Cars.FindCars(Price:60000, Color:"Purple");

            Console.ReadKey();
        }
    }
}
