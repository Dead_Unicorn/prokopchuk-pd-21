﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {

            //R = U/I
            int voltage = int.Parse(args[0],CultureInfo.InvariantCulture);//Console.ReadLine());
            int current = int.Parse(args[1], CultureInfo.InvariantCulture);//Console.ReadLine());
            double resistance = (double)voltage / current;
            Console.WriteLine(resistance.ToString(CultureInfo.InvariantCulture));


        }
    }
}
