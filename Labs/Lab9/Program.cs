﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9
{
    public abstract class Shape : IDraw
    {
        public string Color { get; set; }
        public int Vertexes { get; protected set; }
        public string Name { get; protected set; }
        public abstract void Draw();
        abstract protected double Area();
        abstract protected double Perimeter();
    }

    class Square : Shape
    {
        public Square()
        {
            Random r = new Random();
            Vertexes = 4;
            Name = "Square" + (r).Next(0, int.MaxValue);
            Color = ((ConsoleColor)r.Next(0, 14)).ToString();
            SideLength = ((double)r.Next(0, 50)) + r.NextDouble();
        }
        public Square(string name)
        {
            Random r = new Random();
            Name = name;
            SideLength = ((double)r.Next(0, 50))+ r.NextDouble();
            Color = ((ConsoleColor)r.Next(0, 14)).ToString();
        }
        public Square(string name, double length)
        {
            Random r = new Random();
            Name = name;
            SideLength = length;
            Color = ((ConsoleColor)r.Next(0, 14)).ToString();
        }
        public Square(string name, double length,string color)
        {
            Name = name;
            SideLength = length;
            Color = color;
        }
        public double SideLength { get; set; }
        public override void Draw()
        {
            Console.WriteLine(Name);
            ConsoleColor temp = Console.ForegroundColor;
            Console.ForegroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), Color);
            Console.WriteLine(SideLength);
            Console.ForegroundColor = temp;
        }
        protected override double Area()
        {
            return SideLength * 2;
        }
        protected override double Perimeter()
        {
            return SideLength * 4;
        }
    }

    class Circle : Shape
    {
        public Circle()
        {
            Random r = new Random();
            Vertexes = 1;
            Name = "Circle" + (r).Next(0, int.MaxValue);
            Radius = ((double)r.Next(0, 50)) + r.NextDouble();
            Color = ((ConsoleColor)r.Next(0, 14)).ToString();
        }
        public Circle(string name)
        {
            Random r = new Random();
            Name = name;
            Radius = ((double)r.Next(0, 50)) + r.NextDouble();
            Color = ((ConsoleColor)r.Next(0, 14)).ToString();
        }
        public Circle(string name, double length)
        {
            Random r = new Random();
            Name = name;
            Radius = length;
            Color = ((ConsoleColor)r.Next(0, 14)).ToString();
        }
        public Circle(string name, double radius, string color)
        {
            Name = name;
            Radius = radius;
            Color = color;
        }
        public double Radius { get; set; }
        public override void Draw()
        {
            Console.WriteLine(Name);
            ConsoleColor temp = Console.ForegroundColor;
            Console.ForegroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), Color);
            Console.WriteLine(Radius);
            Console.ForegroundColor = temp;
        }
        protected override double Area()
        {
            return Math.PI * Radius * Radius;
        }
        protected override double Perimeter()
        {
            return 2 * Math.PI * Radius;
        }
    }

    class Triangle : Shape
    {
        public Triangle()
        {
            Random r = new Random();
            Vertexes = 3;
            Name = "Triangle" + (r).Next(0, int.MaxValue);
            Side1 = ((double)r.Next(0, 50)) + r.NextDouble();
            Side2 = ((double)r.Next(0, 50)) + r.NextDouble();
            Side3 = ((double)r.Next(0, 50)) + r.NextDouble();
            Color = ((ConsoleColor)r.Next(0, 14)).ToString();
        }
        public Triangle(string name)
        {
            Random r = new Random();
            Name = name;
            Side1 = ((double)r.Next(0, 50)) + r.NextDouble();
            Side2 = ((double)r.Next(0, 50)) + r.NextDouble();
            Side3 = ((double)r.Next(0, 50)) + r.NextDouble();
            Color = ((ConsoleColor)r.Next(0, 14)).ToString();
        }
        public Triangle(string name, double[] length)
        {
            Random r = new Random();
            Name = name;
            Side1 = length[0];
            Side2 = length[1];
            Side3 = length[2];
            Color = ((ConsoleColor)r.Next(0, 14)).ToString();
        }
        public Triangle(string name, double[] length, string color)
        {
            Name = name;
            Side1 = length[0];
            Side2 = length[1];
            Side3 = length[2];
            Color = color;
        }
        public double Side1 { get; set; }
        public double Side2 { get; set; }
        public double Side3 { get; set; }
        public override void Draw()
        {
            Console.WriteLine(Name);
            ConsoleColor temp = Console.ForegroundColor;
            Console.ForegroundColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), Color);
            Console.WriteLine($"{Side1}x{Side2}x{Side3}");
            Console.ForegroundColor = temp;
        }
        protected override double Area()
        {
            double halfperim = this.Perimeter()/2;
            return Math.Sqrt(halfperim * (halfperim-Side1) *(halfperim - Side2) *(halfperim - Side3));
        }
        protected override double Perimeter()
        {
            return Side1 + Side2 + Side3;
        }
    }

    class Picture : IDraw
    {
        public List<Shape> Shapes { get; set; }
        public int Length
        {
            get => Shapes.Count;
        }
        public Picture()
        {
            Shapes = new List<Shape>();
        }
        public Picture(int length)
        {
            Shapes = new List<Shape>(length);
        }
        public Picture(List<Shape> Shapes)
        {
            this.Shapes = Shapes;
        }
        public void AddNew(Shape shape)
        {
            Shapes.Add(shape);
        }
        public void DelExistant(string name)
        {
            Shapes.RemoveAll(x => x.Name == name);
        }
        public void DelExistant(Shape type)
        {
            Shapes.Remove(type);
        }

        public void Draw()
        {
            foreach (var shape in Shapes)
                Painter.Paint(shape);
        }

        public Shape this[int index]
        {
            get => Shapes[index];
        }

    }

    public interface IDraw
    {
        void Draw();
    }

    public static class Painter
    {
        public static void Paint(IDraw obj)
        {
            obj.Draw();
        }
    }


    class Program
    {
        static List<Shape> shapes = new List<Shape>
        {
            new Circle(),
            new Square(),
            new Triangle(),
            new Circle(),
            new Square(),
            new Triangle(),
            new Circle(),
            new Square(),
            new Triangle()
        };
        static void Main(string[] args)
        {
            Picture pic = new Picture(shapes);
            pic.Draw();
            Console.ReadKey();
        }
    }
}
