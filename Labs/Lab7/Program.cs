﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            int inputNum = -1;
            List<int> binaryList = new List<int>();
            do
            {
                Console.WriteLine("Input 1 or 0 (-1 to exit)");
                try
                {
                    inputNum = Convert.ToInt32(Console.ReadLine());
                }
                catch(Exception)
                {
                    continue;
                }
                if (inputNum < -1 || inputNum > 1)
                {
                    Console.WriteLine("Wrong Value!");
                    continue;
                }
                Console.WriteLine($"Value {inputNum} added");    
                binaryList.Add(inputNum);

            } while(inputNum != -1);
            binaryList.Remove(-1);
            int ones   = binaryList.Count(x => x == 1);
            int zeroes = binaryList.Count(x => x == 0);

            int[] binArray = binaryList.ToArray();



        }   
        
    }
}
