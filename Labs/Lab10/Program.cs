﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab10
{
    internal static class Extensions
    {
    
        public static void ChangeMinMax(this int[] arr)
        {
            int max = arr.Max();
            int min = arr.Min();

            int maxPos = Array.FindIndex(arr,x=>x == max);
            int minPos = Array.FindIndex(arr,x=>x == min);

            arr[minPos] = max;
            arr[maxPos] = min;
            
        }
    
    }

    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 1, 2, 3, 45, 5 };
            arr.ChangeMinMax();
            foreach(var i in arr)
                Console.WriteLine(i);
        }
    }
}
