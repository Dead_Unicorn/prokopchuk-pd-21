﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab11
{
    delegate void HospitalDelegate(Patient patient);
    
    internal static class Hospital
    {
        public static HospitalDelegate KTAndMRT, USAndKTAndRewmo, RengenAndENT, ALL;
        static Hospital()
        {
            KTAndMRT = KT;
            KTAndMRT += MRT;
            USAndKTAndRewmo = US;
            USAndKTAndRewmo += KT ;
            USAndKTAndRewmo += Rewmo;
            RengenAndENT = Rengen;
            RengenAndENT += ENT;
            ALL = MRT;
            ALL += KT;
            ALL += Rengen;
            ALL += ENT;
            ALL += US;
            ALL += Rewmo;
        }
        public static void MRT(Patient patient)
        {
            patient.MRTPassed = true;
        }
        public static void KT(Patient patient)
        {
            patient.KTPassed = true;
        }
        public static void Rengen(Patient patient)
        {
            patient.RengenPassed = true;
        }
        public static void ENT(Patient patient)
        {
            patient.ENTPassed = true;
        }
        public static void US(Patient patient)
        {
            patient.UltraSoundPassed = true;
        }
        public static void Rewmo(Patient patient)
        {
            patient.RewmoProbesPassed = true;
        }
    }
    class Patient
    {
        public bool MRTPassed { get; set; }
        public bool KTPassed { get; set; }
        public bool RengenPassed { get; set; }
        public bool ENTPassed { get; set; }
        public bool UltraSoundPassed { get; set; }
        public bool RewmoProbesPassed { get; set; }
        public override string ToString()
        {
            return $"MRTPassed:{MRTPassed}\nKTPassed:{KTPassed}\nRengenPassed:{RengenPassed}\nENTPassed:{ENTPassed}\nUltraSoundPassed:{UltraSoundPassed}\nRewmoProbesPassed:{RewmoProbesPassed}\n";
        }
    }

    /*  Задание 2 */
    public delegate bool StudentPredicateDelegate(Student student);
    public static class Extension
    {
        public static List<Student> FindStudent(this List<Student> studList, StudentPredicateDelegate pred)
        {
            List<Student> temp = new List<Student>();
            foreach (var student in studList)
                if (pred(student)) temp.Add(student);
            return temp;
        }
    }
    public class Student
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public  bool Adult(Student student) => student.Age >= 18;
        public  bool NameBeginswithA(Student student) => student.Name[0] == 'A';
        public  bool LastNameLongerThree(Student student) => student.LastName.Length > 3;
        public override string ToString()
        {
            return $"Name: {Name}, LastName: {LastName}, Age: {Age}";
        }
    }

    


    class Program
    {
        static void Main(string[] args)
        {
            Patient patient0 = new Patient();
            Patient patient1 = new Patient();
            Patient patient2 = new Patient();
            Patient patient3 = new Patient();
            Patient patient4 = new Patient();
            Console.WriteLine(patient0);
            Hospital.KTAndMRT.Invoke(patient1);
            Console.WriteLine(patient1);
            Hospital.USAndKTAndRewmo.Invoke(patient2);
            Console.WriteLine(patient2);
            Hospital.RengenAndENT.Invoke(patient3);
            Console.WriteLine(patient3);
            Hospital.ALL.Invoke(patient4);
            Console.WriteLine(patient4);


            /* Task 2*/
            List<Student> studlist = new List<Student>
            { 
                new Student{ Name="Ivan",LastName="Petrov",Age=10 },
                new Student{ Name="Artem",LastName="Antonov",Age=15},
                new Student{ Name="Ignat",LastName="Petrov",Age=18},
                new Student{ Name="Ivan",LastName="Ivanov",Age=20},
                new Student{ Name="Maxim",LastName="Kac",Age=15},
                new Student{ Name="Angrey",LastName="Maximov",Age=19},
                new Student{ Name="Killa",LastName="Kirrilova",Age=21},
                new Student{ Name="Andrew",LastName="wha",Age=22},
                new Student{ Name="Somebody",LastName="Troelsen",Age=16},
                new Student{ Name="Maxim",LastName="Ageev",Age=17}
            };
            List<Student> resList = studlist.FindStudent(x => x.Adult(x));
            Console.WriteLine("Adults:");
            foreach(var student in resList)
                Console.WriteLine(student);
            Console.WriteLine();
            Console.WriteLine("Name on A"); 
            resList = studlist.FindStudent(x => x.NameBeginswithA(x));
            foreach (var student in resList)
                Console.WriteLine(student);
            Console.WriteLine();
            Console.WriteLine("LastName longe than 3:");
            resList = studlist.FindStudent(x => x.LastNameLongerThree(x));
            foreach (var student in resList)
                Console.WriteLine(student);
            Console.WriteLine();
            Console.WriteLine("Age between 20 and 25");
            resList = studlist.Where(x => x.Age >= 20 && x.Age <= 25).ToList();
            foreach (var student in resList)
                Console.WriteLine(student);
            Console.WriteLine();
            Console.WriteLine("Name is Andrew");
            resList = (List<Student>)studlist.Where(x=>x.Name=="Andrew").ToList();
            foreach (var student in resList)
                Console.WriteLine(student);
            Console.WriteLine();
            Console.WriteLine("LastName is troelsen");
            resList = (List<Student>)studlist.Where(x => x.LastName == "Troelsen").ToList();
            foreach (var student in resList)
                Console.WriteLine(student);

        }
    }
}
