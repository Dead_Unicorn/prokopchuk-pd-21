﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5
{
    class Program
    {
        static int[,] changeMatrix( int [,] matrix)
        {
            int[,] temp = new int [matrix.GetLength(0),matrix.GetLength(1)];
            for (int k = 0; k < matrix.GetLength(0);++k)
            {
                for (int i = matrix.GetLength(0) - k + 1; i > 0; --i)
                {
                    for (int j = 0; j < matrix.GetLength(1)  /*k > 0*/; ++j)
                    {
                        temp[k, j] = matrix[i--, k];
                    }
                }
            }
            return temp;
        }

        static void Main(string[] args)
        {

            /*Дана квадратна матриця порядку M. Повернути її елементи на 1) 90 2) 180; 3) 270 градусів в додатному напрямку. */
            Console.WriteLine("Введите M");
            int M = Convert.ToInt32(Console.ReadLine());
            //int[,] matrix = new int[M,M];
            int[,] matrix = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
            matrix = changeMatrix(matrix);

            for (int i = 0; i < matrix.GetLength(0); ++i)
            {
                for (int j = 0; j < matrix.GetLength(1); ++j)
                {
                    Console.Write(matrix[i,j]);
                }
                Console.WriteLine();
            }
            
        }
    }
}
