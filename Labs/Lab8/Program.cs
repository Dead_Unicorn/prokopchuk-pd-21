﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Garage
    {
        Auto[] autos = new Auto[10];
        int capacity;
        public int Capacity { get => capacity; }
        public void Add(Auto auto)
        {
            capacity++;
            if (capacity == autos.Length)
            {
                Array.Resize(ref autos,capacity*2);
            }
            autos[capacity] = auto;
        }
        public void Drop(Auto auto)
        {
            int index = Array.IndexOf(autos, auto);
            if (index == -1)
                return;

            autos[index] = autos[capacity];
            Array.Resize(ref autos, capacity - 1);
        }
        public void Search(string template)
        {
            //string[] carParams = template.Split(' ');
            foreach( var auto in Array.FindAll(autos, x => x.ToString().Contains(template)))
            {
                Console.WriteLine(auto);
            }
        }
        public Auto this[int index]
        {
            get => autos[index];
        }
    }
    class Auto
    {
        public string Name { get; set; }
        public string Year { get; set; }
        public string Speed { get; set; }
        public string Color { get; set; }
        public override string ToString()
        {
            return $"Name: {Name} Year:{Year} Speed:{Speed} Color:{Color}";
        }
    }


    class DiskTelephone
    {
        public string PhoneNumber{ get; set; }
        public string AvailableChars { get; set; } = "1234567890";
        public void MakeCall(string number)
        {
            //Some Code...
        }
        public void GetCall(string number)
        {
            //Some Code...
        }
    }
    class ButtonPhone : DiskTelephone
    {
        public new string AvailableChars { get; set; } = "1234567890*#";
        public new void GetCall(string number)
        {
            Console.WriteLine(number);
            //Some Code...
        }
    }

    class MobileBlackWhite : ButtonPhone
    {
        public string Color { get; set; }
        public new string AvailableChars { get; set; } = "1234567890*#$%&@~+-";
        public string ScreenSize{ get; set; }
        public string PhysicalScreenSize{ get; set; }
        public void SendSMS(string number)
        {
            //Some Code...
        }
        public void ReceiveSMS(string number)
        {
            //Some Code...
        }
    }

    class MobileColorful : MobileBlackWhite
    {
        public string[] AvailableColors{ get; set; }
        public bool TwoCards { get; }
        string secondNumber;
        public string SecondNumber
        {
            get => secondNumber;
            set
            {
                if (TwoCards)
                    secondNumber = value;
            }
        
        }
        public void SendMMS(string number)
        {
            //Some Code...
        }
        public void ReceiveMMS(string number)
        {
            //Some Code...
        }
    }

    class Smartphone : MobileColorful
    {
        public bool HasSensor { get; set; }
        public int MaxTouches { get; set; }
        public int NumCameras { get; set; }
        IntPtr CameraHandler;
        public void CreatePhoto(IntPtr CameraHandler)
        {

        }
        public void Createvideo(IntPtr CameraHandler)
        {

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
