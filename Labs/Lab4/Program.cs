﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
             * Дано дійсне число R і масив розміру N. Знайти два елементи масиву, сума яких найближча (найдальша) від даного числа. 
             *
             **/
            Console.WriteLine("Bведите R");
            double R = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Bведите размер массива");
            int N = Convert.ToInt32(Console.ReadLine());
            Random rand = new Random();
            double[] arr = new double[N];
            for (int i = 0; i < arr.Length; ++i)
                arr[i] = rand.Next((int)R) + rand.NextDouble();
          

            Array.Sort(arr);

            double FirstMin = arr.Min(), SecondMin = arr.Min();
            double FirstMax = arr.Max(), SecondMax = arr.Max();
            double firstDiff = arr.Max(), SecondDiff = arr.Min();
            foreach (var a in arr)
                Console.Write(a + " ");
            Console.WriteLine(  );

            for (int i = 0; i < arr.Length; ++i)
            {
               
                for(int j = 1; j < arr.Length - 1;++j)
                {
                    
                    if (firstDiff < (R - arr[i] + arr[j]))
                    {
                        firstDiff = R - arr[i] + arr[j];
                        FirstMin = arr[i];
                        SecondMin = arr[j];

                    }
                    if (firstDiff > (R - arr[i] + arr[j]))
                    {
                        SecondDiff = R - arr[i] + arr[j];
                        FirstMax = arr[i];
                        SecondMax = arr[j];

                    }

                }

            }
            Console.WriteLine("=================================");
            Console.WriteLine(FirstMin);
            Console.WriteLine(SecondMin);
            Console.WriteLine("=================================");
            Console.WriteLine(FirstMax);
            Console.WriteLine(SecondMax);
        }
    }
}
