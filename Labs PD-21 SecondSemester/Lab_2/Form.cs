﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Lab_2
{
    public partial class MainForm : Form
    {
        private const int ButtonCounter = 16;
        private readonly List<int> _numbers;
        private readonly Random _random;
        private readonly Stopwatch _stopWatch;
        private readonly HashSet<int> _tempNumbers;
        private int _numberNow = 1;

        public MainForm() {
            InitializeComponent();
            _numbers     = new List<int>(ButtonCounter);
            _tempNumbers = new HashSet<int>(ButtonCounter);
            _random      = new Random();
            _stopWatch   = new Stopwatch();
            CreateButtons();
        }

        private int GetRandomNumber(int min, int max) {
            int number;
            do {
                int index = _random.Next(min, max);
                number = _numbers[index];
            } while (_tempNumbers.Contains(number));

            return number;
        }
        private void RemoveButton_Click(object sender, EventArgs e) {
            if (ComboBox.Items.Contains(ComboBox.SelectedItem)) {
                ComboBox.Items.Remove(ComboBox.SelectedItem);
                TextBox.BackColor = Color.White;
                TextBox.ForeColor = Color.Black;
                TextBox.Text      = string.Empty;
            }
            else {
                MessageBox.Show("Такого тексту немає.");
                TextBox.BackColor = Color.Red;
                TextBox.ForeColor = Color.White;
            }
        }

        private void CreateButtons() {
            _numbers.AddRange(Enumerable.Range(1, ButtonCounter));

            int size = 30;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < ButtonCounter / 4; j++) {
                    Button button = new Button {
                        Location = new Point((size + 10) * i, (size + 10) * j), Size = new Size(size, size)
                    };
                    button.Click += ButtonClick;
                    int number = GetRandomNumber(0, ButtonCounter);
                    _tempNumbers.Add(number);

                    button.Text = number.ToString();
                    PanelNumbers.Controls.Add(button);
                }
            }

            _tempNumbers.Clear();
        }

        private void AddButton_Click(object sender, EventArgs e) {
            if (TextBox.Text != string.Empty) {
                ComboBox.Items.Add(TextBox.Text);
                TextBox.BackColor = Color.White;
                TextBox.ForeColor = Color.Black;
                TextBox.Text      = string.Empty;
            }
            else {
                MessageBox.Show("Введіть текст!");
                TextBox.BackColor = Color.Red;
                TextBox.ForeColor = Color.White;
            }
        }


        private void ButtonClick(object sender, EventArgs e) {
            Button button = sender as Button;
            if (_numberNow == int.Parse(button?.Text)) {
                if (_numberNow == 1) {
                    _stopWatch.Start();
                }

                if (_numberNow == 16) {
                    TimeSpan ts          = _stopWatch.Elapsed;
                    string   elapsedTime = $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds / 10:00}";
                    StatusBox.Text = $"Молодець! За {elapsedTime}";
                    _stopWatch.Reset();
                    ResetButton();
                    return;
                }

                button.Visible = false;
                _numberNow++;
                ChangeNumber();
            }
            else {
                ResetButton();
            }
        }

        private void ResetButton() {
            _numberNow = 1;
            PanelNumbers.Controls.Clear();
            CreateButtons();
        }

        private void ChangeNumber() {
            foreach (object item in PanelNumbers?.Controls) {
                Button button = item as Button;
                if (button != null && button.Visible) {
                    int number = GetRandomNumber(_numberNow - 1, ButtonCounter);
                    _tempNumbers.Add(number);
                    button.Text = number.ToString();
                }
            }

            _tempNumbers.Clear();
        }

        private void TextBox_TextChanged(object sender, EventArgs e) {
        }
    }
}