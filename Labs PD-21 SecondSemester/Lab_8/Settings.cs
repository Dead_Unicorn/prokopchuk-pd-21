namespace Lab_8
{
    public class Settings
    {
        public double WidthWindow { get; set; }
        
        public double HeightWindow { get; set; }
        
        public bool FirstCheckBoxChecked { get; set; }
        
        public bool SecondCheckBoxChecked { get; set; }
        
        public string TextBoxContent { get; set; }
    }
}